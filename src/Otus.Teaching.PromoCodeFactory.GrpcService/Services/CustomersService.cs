using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GrpcService.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcService
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersShortMessage> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var shortCustomers = customers.Select(x => new CustomerShortMessage()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var response = new CustomersShortMessage();
            response.Customers.AddRange(shortCustomers);

            return response;
        }

        public override async Task<CustomerMessage> GetCustomer(IdRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer ID parse error."));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found."));

            var response = new CustomerMessage()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            response.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceMessage()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }).ToList());

            return response;
        }

        public override async Task<Empty> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(p => Guid.Parse(p)).ToList());

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer ID parse error."));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found."));

            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(p => Guid.Parse(p)).ToList());

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(IdRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer ID parse error."));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found."));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
